package com.redgeckotech.chatapi;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.MediumTest;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.awaitility.Awaitility.await;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@MediumTest
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentationTest {
    public static final String LOG_TAG = ExampleInstrumentationTest.class.getSimpleName();

    public static final String USERNAME = "testuser";
    public static final String HELLO_WORLD = "hello world";

    public static final String BASE_URL = "https://damp-lake-90950.herokuapp.com/";

    Random rnd = new Random();

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.redgeckotech.chatapi.test", appContext.getPackageName());
    }

    boolean completed = false;

    @Test
    public void testChatApiAndBuilder() throws Exception {
        SimpleChat.Builder builder = new SimpleChat.Builder().baseUrl(BASE_URL);
        assertEquals(BASE_URL, builder.baseUrl);
        assertNotEquals("NotAMatch", builder.baseUrl);

        SimpleChat simpleChat = builder.build();
        assertEquals(BASE_URL, simpleChat.baseUrl);
        assertNotEquals("NotAMatch", simpleChat.baseUrl);
    }

    @Test
    public void testInvalidBaseUrls() throws Exception {
        String baseUrl = null;
        try {
            SimpleChat simpleChat = new SimpleChat.Builder().baseUrl(baseUrl).build();
            fail("Null URL was accepted.");
        } catch (NullPointerException e) {
            // this is expected.
        }

        baseUrl = "x:1:2";
        try {
            SimpleChat simpleChat = new SimpleChat.Builder().baseUrl(baseUrl).build();
            fail("Null URL was accepted.");
        } catch (IllegalArgumentException e) {
            // this is expected.
        }

        baseUrl = "http://foo.com";
        try {
            SimpleChat simpleChat = new SimpleChat.Builder().baseUrl(baseUrl).build();
            fail("Invalid URL was accepted: " + baseUrl);
        } catch (IllegalArgumentException e) {
            // this is expected.
        }
    }

    @Test
    public void queryAccount() throws Exception {
        final SimpleChat simpleChat = new SimpleChat.Builder()
                .baseUrl("https://damp-lake-90950.herokuapp.com/")
                .build();

        Log.d(LOG_TAG, "sending message.");

        final String username = USERNAME + rnd.nextInt();
        final String message = HELLO_WORLD + rnd.nextInt();

        Call<ChatMessage> call2 = simpleChat.sendMessage(username, message, 60);

        // Fetch and print a list of the chat messages
        call2.enqueue(new Callback<ChatMessage>() {
            @Override
            public void onResponse(Call<ChatMessage> call, Response<ChatMessage> response) {

                assertEquals(201, response.code());

                ChatMessage chatMessage = response.body();

                assertNotNull(chatMessage);
                assertNotNull(chatMessage.getId());

                // Now that a message was sent, let's retrieve it and verify that it
                // contains the right message text.
                Log.d(LOG_TAG, "retrieving messages.");


                Call<List<ChatMessage>> getCall = simpleChat.getMessages(username);

                getCall.enqueue(new Callback<List<ChatMessage>>() {
                    @Override
                    public void onResponse(Call<List<ChatMessage>> call,
                                           Response<List<ChatMessage>> response) {

                        assertEquals(200, response.code());

                        List<ChatMessage> messages = response.body();

                        // This may fail if random username actually had messages waiting
                        assertEquals(1, messages.size());

                        ChatMessage chatMessage1 = messages.get(0);

                        assertEquals(message, chatMessage1.getText());
                        assertNotEquals("NotAMatch", chatMessage1.getText());

                        Log.d(LOG_TAG, "completed.");
                        completed = true;
                    }

                    @Override
                    public void onFailure(Call<List<ChatMessage>> call, Throwable t) {
                        Log.e(LOG_TAG, "Could not retrieve messages.", t);
                        fail();

                        completed = true;
                    }
                });
            }

            @Override
            public void onFailure(Call<ChatMessage> call, Throwable t) {
                Log.e(LOG_TAG, "Message was not sent.", t);
                fail();

                completed = true;
            }
        });

        await().atMost(5, TimeUnit.SECONDS)
                .until(newUserIsAdded());

    }

    private Callable<Boolean> newUserIsAdded() {
        return new Callable<Boolean>() {
            public Boolean call() throws Exception {
                return completed;
            }
        };
    }

}