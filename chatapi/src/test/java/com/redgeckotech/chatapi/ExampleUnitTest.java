package com.redgeckotech.chatapi;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    public static final String MESSAGE_ID = "1";
    public static final String USERNAME = "username";
    public static final String HELLO_WORLD = "Hello world";
    public static final int TIMEOUT = 60;

    @Test
    public void chatMessage_isCorrect() throws Exception {
        ChatMessage chatMessage = new ChatMessage(MESSAGE_ID, HELLO_WORLD);

        assertEquals(MESSAGE_ID, chatMessage.getId());
        assertNull(chatMessage.getUsername());
        assertEquals(HELLO_WORLD, chatMessage.getText());
        assertEquals(0, chatMessage.getTimeout());

        chatMessage = new ChatMessage(USERNAME, HELLO_WORLD, TIMEOUT);

        assertNull(chatMessage.getId());
        assertEquals(USERNAME, chatMessage.getUsername());
        assertEquals(HELLO_WORLD, chatMessage.getText());
        assertEquals(TIMEOUT, chatMessage.getTimeout());
    }
}