package com.redgeckotech.chatapi;

public class ChatMessage {
    public String id;
    public String username;
    public String text;
    public int timeout;

    public ChatMessage() {
    }

    public ChatMessage(String id, String text) {
        this.id = id;
        this.text = text;
    }

    public ChatMessage(String username, String text, int timeout) {
        this.username = username;
        this.text = text;
        this.timeout = timeout;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    @Override
    public String toString() {
        return "ChatMessage{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", text='" + text + '\'' +
                ", timeout=" + timeout +
                '}';
    }
}
