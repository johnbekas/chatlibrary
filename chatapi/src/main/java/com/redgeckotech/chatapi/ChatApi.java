package com.redgeckotech.chatapi;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ChatApi {

    @POST("chat")
    Call<ChatMessage> sendMessage(@Body ChatMessage chatMessage);

    @GET("/chat/{username}")
    Call<List<ChatMessage>> getMessages(@Path("username") String username);

}

