package com.redgeckotech.chatapi;

import android.net.Uri;
import android.text.TextUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SimpleChat {

    protected final ChatApi chatApi;
    protected final String baseUrl;

    SimpleChat(String baseUrl) {
        this.baseUrl = baseUrl;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(this.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Create an instance of our Chat API interface.
        this.chatApi = retrofit.create(ChatApi.class);
    }

    /**
     * Retrieve messages for the specified user account.
     *
     * @param username The account username.
     * @return A Retrofit Call object containing a List of {@link ChatMessage}s.
     */
    public Call<List<ChatMessage>> getMessages(String username) {
        if (username == null) {
            throw new NullPointerException("username == null");
        }

        return chatApi.getMessages(username);
    }

    /**
     * Sends a message to the specified user account. Timeout = 60s.
     *
     * @param username The account The recipient of the message. Cannot be null or empty.
     * @param text The content of the message. Cannot be null or empty.
     * @return A Retrofit Call object containing a {@link ChatMessage}.
     */
    public Call<ChatMessage> sendMessage(String username, String text) {
        return sendMessage(username, text, 60);
    }

    /**
     * Sends a message to the specified user account. Timeout = 60s.
     *
     * @param username The account The recipient of the message. Cannot be null or empty.
     * @param text The content of the message. Cannot be null or empty.
     * @param timeout The number of seconds the message should live before expiring
     * @return A Retrofit Call object containing a {@link ChatMessage}.
     */
    public Call<ChatMessage> sendMessage(String username, String text, int timeout) {
        if (TextUtils.isEmpty(username)) {
            throw new IllegalArgumentException("username not specified.");
        }
        if (TextUtils.isEmpty(text)) {
            throw new IllegalArgumentException("text not specified.");
        }

        ChatMessage chatMessage = new ChatMessage(username, text, timeout);

        return chatApi.sendMessage(chatMessage);
    }

    public static final class Builder {
        protected String baseUrl = "https://damp-lake-90950.herokuapp.com/";

        /**
         * Set the API base URL.
         *
         * @param baseUrl The REST API base URL. Must end with '/'
         * @return The modified Builder.
         */
        public Builder baseUrl(String baseUrl) {
            if (baseUrl == null) {
                throw new NullPointerException("baseUrl == null");
            }

            // This will throw an IllegalArgumentException if URL is invalid.
            Uri.parse(baseUrl);

            if (!baseUrl.endsWith("/")) {
                throw new IllegalArgumentException("baseUrl must end in /: " + baseUrl);
            }

            this.baseUrl = baseUrl;
            return this;
        }

        /**
         * Create the {@link SimpleChat} instance using the configured values.
         * For sample use, a default URL is set to https://damp-lake-90950.herokuapp.com/
         *
         * @return The SimpleChat API.
         */
        public SimpleChat build() {
            if (baseUrl == null) {
                throw new IllegalStateException("Base URL required.");
            }

            return new SimpleChat(baseUrl);
        }
    }
}
