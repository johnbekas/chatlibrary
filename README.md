ChatLibrary
===========

A basic REST API library to work with an ephemeral [messaging service][1]. 

NOTE, ChatLibrary does not contain Retrofit2 libraries, but they are required for use. 
Reference the following configuration section. 

Gradle dependency configuration:
```groovy
    compile 'org.bitbucket.johnbekas:ChatLibrary:master-SNAPSHOT'
    compile 'com.squareup.retrofit2:retrofit:2.1.0'
    compile 'com.squareup.retrofit2:converter-gson:2.1.0'
```

Example Usage
-------------

```java
        SimpleChat simpleChat = new SimpleChat.Builder()
                .baseUrl("https://damp-lake-90950.herokuapp.com/")
                .build();

        Call<List<ChatMessage>> call = simpleChat.getMessages("testuser");

        // Fetch and print a list of the chat messages
        call.enqueue(new Callback<List<ChatMessage>>() {
            @Override
            public void onResponse(Call<List<ChatMessage>> call, Response<List<ChatMessage>> response) {

                List<ChatMessage> messages = response.body();

                if (messages != null) {
                    // process message list
                } else {
                    // no messages for user
                }
            }

            @Override
            public void onFailure(Call<List<ChatMessage>> call, final Throwable t) {
                // handle error conditions 
            }
        });
```

[1]: https://www.getpostman.com/collections/c927db98de64e3279fc7
